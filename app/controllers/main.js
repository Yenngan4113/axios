console.log(axios());
const baseURL = "https://6271e18225fed8fcb5ec0cdd.mockapi.io";
function turnOnLoading() {
  document.querySelector("#loading").style.display = "flex";
}
function turnOffLoading() {
  document.querySelector("#loading").style.display = "none";
}
const renderDSSPServ = function () {
  axios({
    url: `${baseURL}/san-pham`,
    method: "GET",
  })
    .then(function (res) {
      xuatDanhSachSanPham(res.data);
      // console.log("res", res);
    })
    .catch(function (err) {
      // console.log("err", err);
    });
};
renderDSSPServ();
const deleteSPServ = function (id) {
  turnOnLoading();
  // console.log(id);
  axios({
    url: `${baseURL}/san-pham/${id}`,
    method: "DELETE",
  })
    .then(function () {
      renderDSSPServ();
      turnOffLoading();
    })
    .catch(function () {
      turnOffLoading();
    });
};
const xuatDanhSachSanPham = function (danhsach) {
  var contentHTML = "";
  danhsach.forEach(function (item) {
    var contentTr = `<tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td>${item.image}</td>
    <td>${item.type ? "Phone" : "Laptop"}</td>
    <td class=" btn btn-danger" onClick ="deleteSPServ(${item.id})">Xóa</td>
    <td class=" btn btn-warning" data-toggle="modal"
    data-target="#myModal" onclick="layInforSPtuID(${item.id})">Sửaaa</td>
    </tr>`;
    contentHTML += contentTr;
  });
  document.querySelector("#tblDanhSachSP").innerHTML = contentHTML;
};

axios({
  url: `${baseURL}/san-pham`,
  method: "GET",
})
  .then(function (res) {
    xuatDanhSachSanPham(res.data);
    // console.log("res", res);
  })
  .catch(function (err) {
    // console.log("err", err);
  });
// then: after getting data from server, what will we do
// catch: if can't get data from server, what will we do???
// ... hell: lồng 2 API, thằng nào muốn chạy sau thì lồng ở trong
// Nhiều API chạy cùng lúc, muốn check status hết nếu accepted hết mới up dữ liệu, có 1 thằng bị rejected sẽ báo lỗi\
// ? "Phone" : "Mobile" : toán tử 3 ngôi
const layInfotuform = function () {
  var name = document.querySelector("#TenSP").value;
  var price = document.querySelector("#GiaSP").value;
  var img = document.querySelector("#HinhSP").value;
  var type = +document.querySelector("#loaiSP").value;
  return new SanPham(name, price, img, type == 1 ? true : false);
};
const themMoiSP = function () {
  // console.log("yes");
  var newSP = layInfotuform();
  turnOnLoading();
  axios({
    url: `${baseURL}/san-pham`,
    method: "POST",
    data: newSP,
  })
    .then(function (res) {
      $("#myModal").modal("hide");
      renderDSSPServ();
      turnOffLoading();
    })
    .catch(function (err) {
      turnOffLoading();
    });
};
function showDataLenForm(data) {
  document.querySelector("#TenSP").value = data.name;
  document.querySelector("#GiaSP").value = data.price;
  document.querySelector("#HinhSP").value = data.image;
  document.querySelector("#loaiSP").value = data.type ? "1" : "0";
}
// Lấy thông tin sản phẩm theo ID
function layInforSPtuID(id) {
  document.querySelector("#btn_add").style.display = "none";
  // console.log({ id });
  axios({
    url: `${baseURL}/san-pham/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showDataLenForm(res.data);
      document.querySelector("#idsp span").innerHTML = res.data.id;
    })
    .catch(function (err) {
      // console.log(err);
    });
}
function capNhatSP() {
  var Idsp = document.querySelector("#idsp span").innerHTML * 1;
  var updatedSanPham = layInfotuform();

  // console.log(Idsp);
  axios({
    url: `${baseURL}/san-pham/${Idsp}`,
    method: "PUT",
    data: updatedSanPham,
  })
    .then(function (res) {
      // console.log("cap nhat tc");
      $("#myModal").modal("hide");
      renderDSSPServ();
    })
    .catch(function (err) {
      // console.log("cap nhat tb");
    });
}
document.querySelector("#btnThemSP").addEventListener("click", function () {
  document.querySelector("#formsp").reset();
  document.querySelector("#idsp").style.display = "none";
});
